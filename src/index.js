var express = require('express');
var cors = require('cors');
var Router = express.Router();

var app = express();
var port = 5000;

app.use(express.json());
app.use(cors());
app.use(Router);

app.listen(port, function () {
  console.log(`Ready!\nListening on port ${port}`);
});

var aspiranteRoutes = require('./routes/Aspirantes');
var zonaRoutes = require("./routes/Zonas");
var carreraRoutes = require('./routes/Carreras');

Router.use('/aspirantes/', aspiranteRoutes);
Router.use('/zonas/',zonaRoutes);
Router.use('/carreras', carreraRoutes);
