const mariadb = require('mariadb');
const pool = mariadb.createPool({
  host: "ec2-54-87-163-194.compute-1.amazonaws.com",
  user: "telematica",
  password:"5XYxwCwwOymY5k0H",
  connectionLimit: 5,
  database:"telematica"
});

var select_from_table = async (table) => {
  let conn;
  let rows = [];
  try{
    conn = await pool.getConnection();
    rows = await conn.query(`SELECT * from ${table}`);
    delete rows.meta;
  }
  catch(err){
    rows = err;
  }
  finally{
    if(conn) conn.end();
  }
  return rows;
}

async function insertAspirante(nombres,apellidos,id_zona,id_carrera){
  let conn;
  try {
    conn = await pool.getConnection();
    await conn.query(`INSERT INTO aspirantes(nombres,apellidos,id_zona,id_carrera) VALUES('${nombres}','${apellidos}','${id_zona}','${id_carrera}')`);
    return "500";
  }
  catch (err) {
    console.log(err);
  }
  finally {
    if (conn) return conn.end();
  }
}

async function getAspirantes_for_mail(){
  let conn;
  let rows = [];
  try{
    conn = await pool.getConnection();
    rows = await conn.query(`SELECT aspirantes.nombres, aspirantes.apellidos, zonas.nombre as zona, carreras.nombre as carrera FROM aspirantes, zonas, carreras WHERE aspirantes.id_zona = zonas.id AND aspirantes.id_carrera = carreras.id ORDER BY nombres`);
    delete rows.meta;
  }
  catch(err){
    rows = err;
  }
  finally{
    if(conn) conn.end();
  }
  return rows;
}

module.exports = {select_from_table, insertAspirante, getAspirantes_for_mail};
